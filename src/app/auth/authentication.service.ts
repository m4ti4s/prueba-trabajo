import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';

import { Credentials, CredentialsService } from './credentials.service';

import firebase from '@firebase/app-compat';

export interface LoginContext {
	username: string;
	password: string;
	remember?: boolean;
}

export interface RespuestaLogin {
	datos?: any,
	valido?: boolean;
};

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
	providedIn: 'root',
})
export class AuthenticationService {

	constructor(private credentialsService: CredentialsService, private authFire: AngularFireAuth) {
	}



	async register(loginContext: LoginContext): Promise<RespuestaLogin> {
		try {
			const respuesta = await this.authFire.createUserWithEmailAndPassword(loginContext.username, loginContext.password);
			const tokenId = await respuesta.user?.getIdToken();
			const credentials: Credentials = { username: respuesta.user?.email !== null ? respuesta.user?.email : loginContext.username, token: tokenId };
			this.credentialsService.setCredentials(credentials);
			const resp: RespuestaLogin = { datos: respuesta, valido: true };
			return resp;
		} catch (error) {
			const resp: RespuestaLogin = { datos: null, valido: false };
			return resp;
		}
	}


	async loginEmail(loginContext: LoginContext): Promise<RespuestaLogin> {

		try {
			const respuesta = await this.authFire.signInWithEmailAndPassword(loginContext.username, loginContext.password);
			const tokenId = await respuesta.user?.getIdToken();
			const credentials: Credentials = { username: respuesta.user?.email !== null ? respuesta.user?.email : loginContext.username, token: tokenId };
			this.credentialsService.setCredentials(credentials);
			const resp: RespuestaLogin = { datos: respuesta, valido: true };
			return resp;
		} catch (error) {
			const resp: RespuestaLogin = { datos: null, valido: false };
			return resp;
		}
	}

	async loginWithGoogle(loginContext: LoginContext): Promise<RespuestaLogin> {
		// Replace by proper authentication call
		try {
			const respuesta = await this.authFire.signInWithPopup(new firebase.auth.GoogleAuthProvider());
			const tokenId = await respuesta.user?.getIdToken();
			const credentials: Credentials = { username: respuesta.user?.email !== null ? respuesta.user?.email : loginContext.username, token: tokenId };
			this.credentialsService.setCredentials(credentials);
			const resp: RespuestaLogin = { datos: respuesta, valido: true };
			return resp;
		} catch (error) {
			const resp: RespuestaLogin = { datos: null, valido: false };
			return resp;
		}
	}


	logout() {
		this.authFire.signOut();
	}

	getUserLoggedInfo() {
		return this.authFire.authState;
	}
}
