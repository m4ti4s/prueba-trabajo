import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, UntilDestroy, untilDestroyed } from '@shared';
import { AuthenticationService, RespuestaLogin } from './authentication.service';

const log = new Logger('Login');

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  isLoading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    this.createForm();
  }

  ngOnInit() { }

  async login() {
    this.isLoading = true;
    const res: RespuestaLogin = await this.authenticationService.loginEmail(this.loginForm.value);
    if (res.valido) {
      this.loginForm.markAsPristine();
      this.isLoading = false;
      this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
    } else {
      this.isLoading = false;
      console.log('error');
    }

  }

  async ingresarConGoogle() {

    const res: RespuestaLogin = await this.authenticationService.loginWithGoogle(this.loginForm.value);
    if (res.valido) {
      this.loginForm.markAsPristine();
      this.isLoading = false;
      this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
    } else {
      this.isLoading = false;
      console.log('error');
    }
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true,
    });
  }
}
